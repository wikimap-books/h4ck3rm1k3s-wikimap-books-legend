use HTML::TableParser;

@reqs = (
    {
	id => \&id,
	hdr => \&header,              # function callback
	row => \&row,                 # function callback
	start => \&start,             # function callback
	end => \&end,                 # function callback
    }
    );

# create parser object
$p = HTML::TableParser->new( \@reqs,
			     { MultiMatch=>1, Decode => 1, Trim => 1, Chomp => 1,  } );
$p->parse_file( "Map_Features.html" );

sub id {
    my ( $id, $line, $udata ) = @_;
    #...
#    warn "ID". $id;
    if ($id > 1)
    {
	return true;
    }
}

# function callbacks
sub start {
    my ( $id, $line, $udata ) = @_;
    #...
#    warn "Start". $line;
}
sub end {
    my ( $id, $line, $udata ) = @_;
    #...
#    warn "End". $line;
}

sub header {
    my ( $id, $line, $cols, $udata ) = @_;
    #...
#    warn "Header". $line;
}

sub row  {
    my ( $id, $line, $cols, $udata ) = @_;
    #...
#    warn "ID".$id;
#    warn "Row".$line;
    print "{{MapFeatureData|".join ("|", (map {s/\|/\;/g; s/\n/ /g; $_} @{$cols})). "}}\n";
}
