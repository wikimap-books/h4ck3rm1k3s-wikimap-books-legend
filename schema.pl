#use XML::LibXML;
use XML::LibXML 1.70;

use Template;

my $tt = Template->new({
    INCLUDE_PATH => 'templates',
    INTERPOLATE  => 1,
}) || die "$Template::ERROR\n";

#download 
#http://spreadsheets.google.com/feeds/cells/0Am70fsptsPF2dENGRWNOQ3lmeU5UbWlORFZTZFpvRWc/od6/public/basic
#$parser = XML::LibXML->new();
use Data::Dumper;
use strict;
use warnings;
my $file=shift;
#$dom = $parser->load_xml(location=>"$file");

# now extract 
  use XML::LibXML::Reader;


sub RowDescription
{
}
my @columns;
sub RowHeader
{
    my $row=shift;
    foreach my $c (@{$row})
    {

	push @columns,$c->{content};
    }
    warn join "|",@columns;
}


sub RowData
{
    my $row=shift;
    my %obj;
    my @data;
    foreach my $c (@{$row})
    {
#	warn Dumper($c);
	push @data,$c->{content};
    }

    foreach my $c (@columns)
    {
	my $v = shift @data||"";
	$obj{$c}=$v;
    }
    #warn join "|",@columns;
#    warn Dumper(\%obj);
    $tt->process('Wiki.tt', \%obj)
	|| die $tt->error(), "\n";

}

my $reader = new XML::LibXML::Reader(
    location => 
    $file||
    "http://spreadsheets.google.com/feeds/cells/0Am70fsptsPF2dENGRWNOQ3lmeU5UbWlORFZTZFpvRWc/od6/public/basic"
    )
    or die "cannot read file.xml\n";
while ($reader->read) {
    processNode($reader);
}

my $state=0;
my $text;
my $object={};
my @row=();
my $row=0;
my $cname="NONAME";

sub processNode {
    $reader = shift;
#  <entry>
 #   <id>https://spreadsheets.google.com/feeds/cells/0Am70fsptsPF2dENGRWNOQ3lmeU5UbWlORFZTZFpvRWc/od6/public/basic/R365C6
 #   <updated>2010-10-29T01:52:41.743Z</updated>
 #   <category scheme="http://schemas.google.com/spreadsheets/2006" term="http://schemas.google.com/spreadsheets/2006#cell"/ #   <title type="text">F365</title>
 #   <content type="text">NOV</content>
#    <link rel="self" type="application/atom+xml" href="https://spreadsheets.google.com/feeds/cells/0Am70fsptsPF2dENGRWNOQ3lmeU5UbWlORFZTZFpvRWc/od6/public/basic/R365C6"/>
#  </entry>
#[id:|1]	[#text:https://spreadsheets.google.com/feeds/cells/0Am70fsptsPF2dENGRWNOQ3lmeU5UbWlORFZTZFpvRWc/od6/public/basic/R51C8|3]	
#[id:|15]	[updated:|1]	[#text:2010-10-29T01:52:41.743Z|3]	[updated:|15]	
#[category:|1]	
#[title:|1]	[#text:H51|3]	[title:|15]	
#[content:|1]	[#text:An unattended electronic machine in a public place, connected to a data system and related equipment and activated by a bank customer to obtain cash withdrawals and other banking services. |3]	[content:|15]
#	[link:|1]	
#[entry:|15]	
    if ($reader->nodeType == 1) #begin
    {
	$cname =$reader->name;
    }
    if ( $reader->name eq "#text")
    {
	$text=$reader->value;
	return;
    }
    if ($reader->nodeType == 15) #end
    {
	$object->{$cname}=$text;
	$text=undef;
	$cname=undef;
    }
    if ($state >= 1  )
    {
	$state =2;
#	printf "[%s:%s|%d]\t", (
#	    $reader->name,
#	    $reader->value,
#	    $reader->nodeType,	    );	
    }
    if ($reader->name eq "entry")
    {
	$state=1;

#	warn Dumper($object);	
	my $atitle =$object->{title};

	if ($atitle)
	{
	    if ($atitle =~ /^([A-Z]*)(\d*)$/)
	    {
		my $ccol=$1;
		my $crow=$2;
		if ($crow ne $row)
		{
		    #print "ROW:". Dumper(\@row);	

		    
		    if ($crow < 3)
		    {
			RowDescription(\@row);
		    }
		    elsif ($crow == 3)
		    {
			RowHeader(\@row);
		    }
		    else
		    {
			RowData(\@row);
		    }

		    @row=();
		}
		$row=$crow;
	    }
	    else
	    {
		warn "Problem:".  Dumper($object);	

	    }
	    push @row,$object;
	    
	}
	$object={};
#	print "\n";

	
    }

}
